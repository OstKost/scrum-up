import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Person } from '../person/person.model';
import { Project } from '../project/project.model';

export const jiraIssueRegex = /^[A-Za-z]{3,6}-\d{3,5}$/;

@ObjectType()
@Schema()
export class Task {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  @Prop({ unique: true, validate: jiraIssueRegex })
  code: string;

  @Field(() => String)
  @Prop({ default: '' })
  title: string;

  @Field(() => String)
  @Prop({ default: '' })
  description: string;

  @Field(() => String)
  @Prop({ enum: ['TODO', 'IN_PROGRESS', 'DONE'], default: 'TODO' })
  status: string;

  @Field(() => Person)
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: Person.name })
  author: Person;

  @Field(() => String)
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: Project.name })
  project: Project;

  @Field(() => Date)
  @Prop({ default: Date.now() })
  createdAt: Date;

  @Field(() => Date)
  @Prop({ default: Date.now() })
  updatedAt: Date;
}

export type TaskDocument = Task & Document;

export const TaskSchema = SchemaFactory.createForClass(Task);
