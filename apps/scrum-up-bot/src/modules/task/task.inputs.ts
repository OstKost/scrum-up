import { Field, InputType } from '@nestjs/graphql';
import { Schema as MongooseSchema } from 'mongoose';

@InputType()
export class ListTaskInput {
  @Field(() => String, { nullable: true })
  code?: string;

  @Field(() => String, { nullable: true })
  author: MongooseSchema.Types.ObjectId;
}

@InputType()
export class CreateTaskInput {
  @Field(() => String)
  code: string;

  @Field(() => String, { nullable: true })
  title?: string;

  @Field(() => String, { nullable: true })
  description?: string;

  @Field(() => String)
  author: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  project: MongooseSchema.Types.ObjectId;

  @Field(() => String, { nullable: true })
  status: string;
}

@InputType()
export class UpdateTaskInput {
  @Field(() => String)
  title: string;

  @Field(() => String)
  description: string;
}
