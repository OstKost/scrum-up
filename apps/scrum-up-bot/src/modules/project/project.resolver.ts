import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { Schema as MongooseSchema } from 'mongoose';
import { Project, ProjectDocument } from './project.model';
import { ProjectService } from './project.service';
import {
  CreateProjectInput,
  ListProjectInput,
  UpdateProjectInput,
} from './project.inputs';
import { Person } from '../person/person.model';

@Resolver(() => Project)
export class ProjectResolver {
  constructor(private projectService: ProjectService) {}

  @Query(() => Project)
  async project(
    @Args('_id', { type: () => String }) _id: MongooseSchema.Types.ObjectId,
  ) {
    return this.projectService.getById(_id);
  }

  @Query(() => [Project])
  async projects(
    @Args('filters', { nullable: true }) filters?: ListProjectInput,
  ) {
    return this.projectService.list(filters);
  }

  @Mutation(() => Project)
  async createProject(@Args('payload') payload: CreateProjectInput) {
    return this.projectService.create(payload);
  }

  // TODO: owner only
  @Mutation(() => Project)
  async updateProject(@Args('payload') payload: UpdateProjectInput) {
    return this.projectService.update(payload);
  }

  @ResolveField(() => Person)
  async owner(
    @Parent() project: ProjectDocument,
    @Args('populate', { defaultValue: false }) populate: boolean,
  ) {
    if (populate) await project.populate({ path: 'owner', model: Person.name });
    return project.owner;
  }
}
