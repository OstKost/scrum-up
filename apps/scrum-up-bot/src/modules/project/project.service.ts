import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema as MongooseSchema } from 'mongoose';

import { Project, ProjectDocument } from './project.model';
import {
  CreateProjectInput,
  ListProjectInput,
  UpdateProjectInput,
} from './project.inputs';
import { Person, PersonDocument } from '../person/person.model';

@Injectable()
export class ProjectService {
  constructor(
    @InjectModel(Project.name) private projectModel: Model<ProjectDocument>,
    @InjectModel(Person.name) private personModel: Model<PersonDocument>,
  ) {}

  create(payload: CreateProjectInput) {
    const createdProject = new this.projectModel(payload);
    return createdProject.save();
  }

  getById(_id: MongooseSchema.Types.ObjectId) {
    return this.projectModel.findById(_id).exec();
  }

  getByCode(code: string) {
    return this.projectModel.findOne({ code }).exec();
  }

  list(filters: ListProjectInput) {
    return this.projectModel
      .find({ ...filters })
      .populate({ path: 'users', model: Person.name })
      .exec();
  }

  update(payload: UpdateProjectInput) {
    return this.projectModel
      .findByIdAndUpdate(payload._id, payload, { new: true })
      .populate({ path: 'users', model: Person.name });
  }

  delete(_id: MongooseSchema.Types.ObjectId) {
    return this.projectModel.findByIdAndDelete(_id).exec();
  }

  async addUser(payload: {
    project: MongooseSchema.Types.ObjectId;
    user: MongooseSchema.Types.ObjectId;
  }) {
    const person = await this.personModel.findById(payload.user);
    return this.projectModel
      .findOneAndUpdate(
        { _id: payload.project },
        { $addToSet: { users: person._id } },
        { returnDocument: 'after' },
      )
      .populate({ path: 'users', model: Person.name });
  }

  removeUser(payload: {
    project: MongooseSchema.Types.ObjectId;
    user: MongooseSchema.Types.ObjectId;
  }) {
    return this.projectModel
      .findOneAndUpdate(
        { _id: payload.project },
        { $pull: { users: payload.user } },
        { returnDocument: 'after' },
      )
      .populate({ path: 'users', model: Person.name });
  }
}
