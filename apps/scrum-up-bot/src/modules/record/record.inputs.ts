import { Field, InputType } from '@nestjs/graphql';
import dayjs from 'dayjs';
import { Schema as MongooseSchema } from 'mongoose';
import { Person } from '../person/person.model';

const dayStart = () => dayjs().startOf('day').toDate();
const dayEnd = () => dayjs().endOf('day').toDate();

@InputType()
export class ListRecordInput {
  @Field(() => Date, { defaultValue: dayStart() })
  createDateStart?: string;

  @Field(() => Date, { defaultValue: dayEnd() })
  createDateEnd?: string;

  @Field(() => String, { nullable: true })
  author?: MongooseSchema.Types.ObjectId;

  @Field(() => String, { nullable: true })
  project?: string;
}

@InputType()
export class CreateRecordInput {
  @Field(() => String)
  author: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  project: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  task: string;

  @Field(() => String)
  status: string;

  @Field(() => String, { nullable: true })
  text?: string;
}

@InputType()
export class UpdateRecordInput {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  author: Person;

  @Field(() => String)
  project: string;

  @Field(() => String)
  text: string;
}
