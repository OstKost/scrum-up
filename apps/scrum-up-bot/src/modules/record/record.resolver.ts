import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { RecordService } from './record.service';
import { Record, RecordDocument } from './record.model';
import {
  CreateRecordInput,
  ListRecordInput,
  UpdateRecordInput,
} from './record.inputs';
import { Project } from '../project/project.model';
import { Person, PersonDocument } from '../person/person.model';

@Resolver(() => Record)
export class RecordResolver {
  constructor(private projectService: RecordService) {}

  @Query(() => [Record])
  async records(
    @Args('filters', { nullable: true }) filters?: ListRecordInput,
  ) {
    return this.projectService.list(filters);
  }

  @Mutation(() => Record)
  async createRecord(@Args('payload') payload: CreateRecordInput) {
    return this.projectService.create(payload);
  }

  @Mutation(() => Record)
  async updateRecord(@Args('payload') payload: UpdateRecordInput) {
    return this.projectService.update(payload);
  }

  @ResolveField(() => Record)
  async author(@Parent() record: RecordDocument) {
    await record.populate({ path: 'author', model: Person.name });
    return record.author;
  }
}
