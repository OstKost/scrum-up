import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema as MongooseSchema } from 'mongoose';

import { Project } from '../project/project.model';

@ObjectType()
@Schema({ collection: 'persons' })
export class Person {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  @Prop({ unique: true })
  username: string;

  @Field(() => String)
  @Prop()
  password: string;

  @Field(() => Number)
  @Prop()
  chatId: number;

  @Field(() => String)
  @Prop({ default: '' })
  first_name: string;

  @Field(() => String)
  @Prop({ default: '' })
  last_name: string;

  @Field(() => String)
  @Prop({ default: '' })
  email: string;

  @Field(() => String)
  @Prop({ default: '' })
  phone: string;

  @Field(() => [Project])
  @Prop({ type: [MongooseSchema.Types.ObjectId], ref: 'Project' })
  projects: MongooseSchema.Types.ObjectId[] | Project[];
}

export type PersonDocument = Person & Document;

export const PersonSchema = SchemaFactory.createForClass(Person);
