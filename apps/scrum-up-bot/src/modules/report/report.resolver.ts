import { Args, Query, Resolver } from '@nestjs/graphql';
import { Record } from '../record/record.model';
import { RecordService } from '../record/record.service';
import { ListRecordInput } from '../record/record.inputs';

@Resolver(() => Record)
export class ReportResolver {
  constructor(private recordService: RecordService) {}

  @Query(() => [Record])
  async getReport(@Args('payload') payload: ListRecordInput) {
    console.log('DEBUG:', 'report.resolver => payload', payload);
    return this.recordService.list(payload);
  }
}
