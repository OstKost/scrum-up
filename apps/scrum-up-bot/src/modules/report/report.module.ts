import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Record, RecordSchema } from '../record/record.model';
import { RecordService } from '../record/record.service';
import { ReportResolver } from './report.resolver';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Record.name, schema: RecordSchema }]),
  ],
  providers: [RecordService, ReportResolver],
})
export class ReportModule {}
