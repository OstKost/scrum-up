import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Project, ProjectSchema } from '../modules/project/project.model';
import { ProjectUpdate } from './project.update';
import { ProjectListScene } from './scenes/project-list.scene';
import { Person, PersonSchema } from '../modules/person/person.model';
import { PersonService } from '../modules/person/person.service';
import { ProjectService } from '../modules/project/project.service';
import { ProjectCardScene } from './scenes/project-card.scene';
import { ProjectCreateScene } from './scenes/project-create.scene';
import { ProjectUpdateScene } from './scenes/project-update.scene';
import { ProjectUsersScene } from './scenes/project-users.scene';
import { ProjectAddUserScene } from './scenes/project-add-user.scene';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Project.name,
        schema: ProjectSchema,
      },
      {
        name: Person.name,
        schema: PersonSchema,
      },
    ]),
  ],
  providers: [
    ProjectUpdate,
    PersonService,
    ProjectService,
    ProjectListScene,
    ProjectCardScene,
    ProjectCreateScene,
    ProjectUpdateScene,
    ProjectUsersScene,
    ProjectAddUserScene,
  ],
})
export class ProjectModule {}
