import { Ctx, Message, SceneEnter, Wizard, WizardStep } from 'nestjs-telegraf';
import { Scenes } from 'telegraf';
import { InjectConnection } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

import { Project } from '../../modules/project/project.model';
import { PersonService } from '../../modules/person/person.service';
import { ProjectService } from '../../modules/project/project.service';
import { TMessage } from '../../utils/types';
import {
  PROJECT_ADD_USER_SCENE_ID,
  PROJECT_USERS_SCENE_ID,
} from '../project.constants';

@Wizard(PROJECT_ADD_USER_SCENE_ID)
export class ProjectAddUserScene {
  constructor(
    @InjectConnection() private readonly connection: mongoose.Connection,
    private personService: PersonService,
    private projectService: ProjectService,
  ) {}

  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Scenes.SceneContext) {
    await ctx.reply(`🤷 Введите никнейм (username) пользователя:`);
    ctx.session['user_form'] = {
      username: '',
    };
  }

  @WizardStep(0)
  async setUsername(
    @Ctx() ctx: Scenes.WizardContext,
    @Message() message: TMessage,
  ) {
    const project: Project = ctx.session['project'];
    if (!project) return ctx.reply(`Проект не найден`);

    const username = message?.text ?? '';
    if (!username) return ctx.reply('⚠️ Введите никнейм пользователя');

    let user = await this.personService.getByUsername(username);
    if (!user) {
      user = await this.personService.create({ username });
      await ctx.reply(
        '⚠️ Пользователь не регистрировался, но теперь заведен в базу',
      );
    }

    const transactionSession = await this.connection.startSession();
    transactionSession.startTransaction();
    const newProject = await this.projectService.addUser({
      user: user._id,
      project: project._id,
    });
    await this.personService.addProject({
      code: project.code,
      person: user._id,
    });
    await transactionSession.endSession();

    if (newProject?._id) ctx.session['project'] = newProject;
    await ctx.reply('✅ Пользователь добавлен');
    await ctx.scene.enter(PROJECT_USERS_SCENE_ID);
  }
}
