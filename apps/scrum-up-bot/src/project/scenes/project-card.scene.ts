import { Action, Ctx, Scene, SceneEnter } from 'nestjs-telegraf';
import { Markup, Scenes } from 'telegraf';

import { Project } from '../../modules/project/project.model';
import {
  PROJECT_CARD_SCENE_ID,
  PROJECT_LIST_SCENE_ID,
  PROJECT_UPDATE_SCENE_ID,
  PROJECT_USERS_SCENE_ID,
  TO_PROJECT_USERS_ACTION,
  TO_PROJECTS_ACTION,
  TO_UPDATE_PROJECT_ACTION,
} from '../project.constants';
import { UpdateContext } from '../../utils/types';

@Scene(PROJECT_CARD_SCENE_ID)
export class ProjectCardScene {
  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Scenes.SceneContext) {
    const project: Project = ctx.session['project'];
    if (!project) {
      return ctx.reply(`Проект не найден`);
    }

    const infoMsg = `ℹ️ Информация по проекту:
      Код:  ${project.code}
      Имя:  ${project.name}
      Участников: ${project.users.length}
      Описание:  ${project.description}`;
    const keyboard = Markup.inlineKeyboard(
      [
        Markup.button.callback('✏️ Изменить', TO_UPDATE_PROJECT_ACTION),
        Markup.button.callback(
          `🧑‍💻 Участники (${project.users.length})`,
          TO_PROJECT_USERS_ACTION,
        ),
        Markup.button.callback(`🔙 Назад к списку`, TO_PROJECTS_ACTION),
      ],
      { columns: 2 },
    );
    await ctx.reply(infoMsg, keyboard);
  }

  @Action(TO_UPDATE_PROJECT_ACTION)
  async toUpdateProject(@Ctx() ctx: UpdateContext) {
    await ctx.answerCbQuery('Преходим к редактированию');
    await ctx.scene.enter(PROJECT_UPDATE_SCENE_ID);
  }

  @Action(TO_PROJECT_USERS_ACTION)
  async toProjectPersons(@Ctx() ctx: UpdateContext) {
    await ctx.answerCbQuery('Преходим к участникам проекта');
    await ctx.scene.enter(PROJECT_USERS_SCENE_ID);
  }

  @Action(TO_PROJECTS_ACTION)
  async backToProjects(@Ctx() ctx: UpdateContext) {
    await ctx.answerCbQuery('Возврат к списку проектов');
    await ctx.scene.enter(PROJECT_LIST_SCENE_ID);
  }
}
