import { Action, Ctx, Scene, SceneEnter } from 'nestjs-telegraf';
import { InjectConnection } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { Markup, Scenes } from 'telegraf';

import {
  PROJECT_ADD_USER_ACTION,
  PROJECT_ADD_USER_SCENE_ID,
  PROJECT_CANCEL_REMOVE_USER_ACTION,
  PROJECT_CARD_SCENE_ID,
  PROJECT_CONFIRM_REMOVE_USER_ACTION,
  PROJECT_LIST_SCENE_ID,
  PROJECT_REMOVE_USER_ACTION,
  PROJECT_USERS_SCENE_ID,
  TO_PROJECT_CARD_ACTION,
  TO_PROJECTS_ACTION,
} from '../project.constants';
import { Project } from '../../modules/project/project.model';
import { Person } from '../../modules/person/person.model';
import { getUserAnswer } from '../../utils/helpers';
import { UpdateContext } from '../../utils/types';
import { PersonService } from '../../modules/person/person.service';
import { ProjectService } from '../../modules/project/project.service';

@Scene(PROJECT_USERS_SCENE_ID)
export class ProjectUsersScene {
  constructor(
    @InjectConnection() private readonly connection: mongoose.Connection,
    private personService: PersonService,
    private projectService: ProjectService,
  ) {}

  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Scenes.SceneContext) {
    const project: Project = ctx.session['project'];
    console.log('ProjectUsersScene', project);
    const users = (project?.users || []) as Person[];
    if (!project) return ctx.reply(`Проект не найден`);

    const bottomButtons = Markup.inlineKeyboard(
      [
        Markup.button.callback(
          '➕  Добавить пользователя',
          PROJECT_ADD_USER_ACTION,
        ),
        Markup.button.callback('🔙 Назад к проекту', TO_PROJECT_CARD_ACTION),
        Markup.button.callback('📄 К списку проектов', TO_PROJECTS_ACTION),
      ],
      { columns: 2, wrap: (btn, index) => index < 2 },
    );

    if (!users.length) {
      const emptyMsg = '⚠️ У проекта нет пользователей';
      return ctx.reply(emptyMsg, bottomButtons);
    }

    for (const user of users) {
      const userKeyboard = Markup.inlineKeyboard([
        Markup.button.callback(
          '❌ Удалить пользователя',
          `${PROJECT_REMOVE_USER_ACTION}@${user.username}`,
        ),
      ]);
      await ctx.reply(user.username, userKeyboard);
    }

    await ctx.reply('Действия:', bottomButtons);
  }

  @Action(/action:remove_project_user@.*$/)
  async confirmRemoveUser(@Ctx() ctx: UpdateContext) {
    const userAnswer = getUserAnswer(ctx);
    const username = userAnswer.split('@')[1];

    const confirmKeyboard = Markup.inlineKeyboard([
      Markup.button.callback(
        '❗ Да, удалить',
        `${PROJECT_CONFIRM_REMOVE_USER_ACTION}@${username}`,
      ),
      Markup.button.callback(
        '🔙 Отмена',
        `${PROJECT_CANCEL_REMOVE_USER_ACTION}@${username}`,
      ),
    ]);
    await ctx.editMessageText(
      `Удалить из проекта ${username} ⁉️`,
      confirmKeyboard,
    );
  }

  @Action(/^action:confirm_remove_project_user@.*$/)
  async removeUser(@Ctx() ctx: UpdateContext) {
    const userAnswer = getUserAnswer(ctx);
    const username = userAnswer.split('@')[1];
    const project: Project = ctx.session['project'];
    if (!project) return ctx.reply(`Проект не найден`);
    const user = await this.personService.getByUsername(username);
    if (!user) return ctx.reply(`Пользователь ${username} не найден`);

    const transactionSession = await this.connection.startSession();
    transactionSession.startTransaction();
    const newProject = await this.projectService.removeUser({
      user: user._id,
      project: project._id,
    });
    await this.personService.deleteProject({
      code: project.code,
      person: user._id,
    });
    await transactionSession.endSession();

    ctx.session['project'] = newProject;

    await ctx.editMessageText(`Пользователь ${username} удален из проекта`);
  }

  @Action(/action:cancel_remove_project_user@.*$/)
  async cancelRemoveUser(@Ctx() ctx: UpdateContext) {
    const userAnswer = getUserAnswer(ctx);
    const username = userAnswer.split('@')[1];
    const userKeyboard = Markup.inlineKeyboard([
      Markup.button.callback(
        '❌ Удалить пользователя',
        `${PROJECT_REMOVE_USER_ACTION}@${username}`,
      ),
    ]);
    await ctx.editMessageText(username, userKeyboard);
  }

  @Action(TO_PROJECT_CARD_ACTION)
  async backToProject(@Ctx() ctx: Scenes.SceneContext) {
    await ctx.answerCbQuery('Возврат к проекту');
    await ctx.scene.enter(PROJECT_CARD_SCENE_ID);
  }

  @Action(TO_PROJECTS_ACTION)
  async backToProjectList(@Ctx() ctx: Scenes.SceneContext) {
    await ctx.answerCbQuery('Возврат к списку проектов');
    await ctx.scene.enter(PROJECT_LIST_SCENE_ID);
  }

  @Action(PROJECT_ADD_USER_ACTION)
  async addUser(@Ctx() ctx: Scenes.SceneContext) {
    await ctx.answerCbQuery('Переходим к добавлению пользователя в проект');
    await ctx.scene.enter(PROJECT_ADD_USER_SCENE_ID);
  }
}
