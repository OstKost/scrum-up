import {
  Action,
  Ctx,
  Message,
  SceneEnter,
  SceneLeave,
  Wizard,
  WizardStep,
} from 'nestjs-telegraf';

import {
  PROJECT_CARD_SCENE_ID,
  PROJECT_UPDATE_SCENE_ID,
  SAVE_PROJECT_ACTION,
  TO_PROJECTS_ACTION,
} from '../project.constants';
import { ProjectService } from '../../modules/project/project.service';
import { Markup, Scenes } from 'telegraf';
import { TMessage, UpdateContext } from '../../utils/types';
import { Project } from '../../modules/project/project.model';

@Wizard(PROJECT_UPDATE_SCENE_ID)
export class ProjectUpdateScene {
  constructor(private projectService: ProjectService) {}

  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Scenes.SceneContext) {
    const project: Project = ctx.session['project'];
    if (!project) return ctx.reply(`Проект не найден`);

    ctx.session['project_form'] = {
      name: '',
      description: '',
    };

    await ctx.reply(`Введите новые данные:`);
    await ctx.reply(`1️⃣ Имя проекта:`);
  }

  @SceneLeave()
  async onSceneLeave(@Ctx() ctx: Scenes.SceneContext) {
    ctx.session['project_form'] = undefined;
  }

  @WizardStep(0)
  async setName(
    @Ctx() ctx: Scenes.WizardContext,
    @Message() message: TMessage,
  ) {
    ctx.session['project_form'].name = message.text;
    await ctx.reply(`2️⃣ Описание проекта:`);
    ctx.wizard.next();
  }

  @WizardStep(1)
  async setDescription(
    @Ctx() ctx: Scenes.WizardContext,
    @Message() message: TMessage,
  ) {
    ctx.session['project_form'].description = message.text;
    const project: Project = ctx.session['project'];
    if (!project) return ctx.reply(`Проект не найден`);
    const form = ctx.session['project_form'];
    const msg = `Соханить изменения в ${project.code} ❔
    🔹 <i>Имя:</i>  <b>${form.name}</b>
    🔹 <i>Описание:</i> <b>${form.description}</b>`;
    const keyboard = Markup.inlineKeyboard(
      [
        Markup.button.callback('✔️ Да', SAVE_PROJECT_ACTION),
        Markup.button.callback('✖️ Нет', TO_PROJECTS_ACTION),
      ],
      { columns: 2 },
    );
    await ctx.replyWithHTML(msg, keyboard);
  }

  @Action(SAVE_PROJECT_ACTION)
  async saveNewProject(@Ctx() ctx: UpdateContext) {
    const form = ctx.session['project_form'];
    const project = ctx.session['project'];
    if (!project) return ctx.reply(`Проект не найден`);
    const successMsg = (p: Project) => `✅ Проект ${p.code} успешно изменен`;
    const errorMsg = (err: string) => `❌ Проект не изменен: ${err}`;
    const payload = { ...form, _id: project?._id };
    const result = await this.projectService
      .update(payload)
      .catch((err) => err.message);
    console.log('update:', result);
    if (result?._id) ctx.session['project'] = result;
    const msg = result?._id ? successMsg(result) : errorMsg(result);
    await ctx.answerCbQuery(msg, { show_alert: !result?._id });
    await ctx.scene.enter(PROJECT_CARD_SCENE_ID);
  }

  @Action(TO_PROJECTS_ACTION || 'cancel')
  async cancelNewProject(@Ctx() ctx: UpdateContext) {
    await ctx.answerCbQuery('Возврат к проекту');
    await ctx.scene.enter(PROJECT_CARD_SCENE_ID);
  }
}
