import {
  Action,
  Ctx,
  Scene,
  SceneEnter,
  SceneLeave,
  Sender,
} from 'nestjs-telegraf';
import { Scenes } from 'telegraf';

import { Project } from '../../modules/project/project.model';
import {
  LEAVE_SCENE_ACTION,
  PROJECT_CARD_SCENE_ID,
  PROJECT_CREATE_SCENE_ID,
  PROJECT_LIST_SCENE_ID,
  TO_CREATE_PROJECT_ACTION,
} from '../project.constants';
import { getUserAnswer } from '../../utils/helpers';
import { PersonService } from '../../modules/person/person.service';
import { ProjectService } from '../../modules/project/project.service';
import { UpdateContext } from '../../utils/types';

@Scene(PROJECT_LIST_SCENE_ID)
export class ProjectListScene {
  constructor(
    private personService: PersonService,
    private projectService: ProjectService,
  ) {}

  @SceneEnter()
  async onSceneEnter(
    @Ctx() ctx: Scenes.SceneContext,
    @Sender('username') username: string,
  ) {
    const owner = await this.personService.getByUsername(username);
    if (!owner) {
      return ctx.reply(`Пользователь ${username} не найден`);
    }

    const projects: Project[] = await this.projectService.list({
      owner: owner._id,
    });
    const bottomButton = [
      {
        text: '➕  Добавить проект',
        callback_data: TO_CREATE_PROJECT_ACTION,
      },
      {
        text: '🔙 Назад',
        callback_data: LEAVE_SCENE_ACTION,
      },
    ];

    if (!projects.length) {
      await ctx.reply(`⚠️ Проекты пользователя ${owner.username} не найдены.`, {
        reply_markup: {
          inline_keyboard: [bottomButton],
        },
      });
      return;
    }

    const projectsKeyboard = projects.map((p) => [
      {
        text: `${p.code}:   ${p.name}`,
        callback_data: String(p.code),
      },
    ]);

    await ctx.reply('📃 Выберите проект:', {
      reply_markup: {
        inline_keyboard: [...projectsKeyboard, bottomButton],
        one_time_keyboard: true,
      },
    });

    ctx.session['projects'] = projects;
  }

  @Action(TO_CREATE_PROJECT_ACTION)
  async toAddProjectScene(@Ctx() ctx: UpdateContext) {
    await ctx.answerCbQuery('Создание проекта');
    await ctx.scene.enter(PROJECT_CREATE_SCENE_ID);
  }

  @Action(LEAVE_SCENE_ACTION)
  async leaveScene(@Ctx() ctx: UpdateContext) {
    await ctx.answerCbQuery('Выходим из сцены');
    await ctx.scene.leave();
  }

  @SceneLeave()
  async onSceneLeave(@Ctx() ctx: UpdateContext) {
    ctx.session['projects'] = undefined;
  }

  @Action(/.*/)
  async onChooseProject(@Ctx() ctx: UpdateContext) {
    const userAnswer = getUserAnswer(ctx);
    await ctx.answerCbQuery(`Вы выбрали ${userAnswer}`, { show_alert: false });

    const projects: [Project] = ctx.session['projects'];
    const project = projects.find((p) => p.code === userAnswer);
    if (!project) {
      return ctx.reply(`Проект с кодом ${userAnswer} не найден`);
    }

    ctx.session['project'] = project;
    await ctx.scene.enter(PROJECT_CARD_SCENE_ID);
  }
}
