import {
  Action,
  Ctx,
  Message,
  SceneEnter,
  SceneLeave,
  Sender,
  Wizard,
  WizardStep,
} from 'nestjs-telegraf';

import {
  PROJECT_CREATE_SCENE_ID,
  PROJECT_LIST_SCENE_ID,
  SAVE_PROJECT_ACTION,
  TO_PROJECTS_ACTION,
} from '../project.constants';
import { Markup, Scenes } from 'telegraf';
import { TMessage, UpdateContext } from '../../utils/types';
import { jiraProjectRegex, Project } from '../../modules/project/project.model';
import { PersonService } from '../../modules/person/person.service';
import { ProjectService } from '../../modules/project/project.service';

@Wizard(PROJECT_CREATE_SCENE_ID)
export class ProjectCreateScene {
  constructor(
    private personService: PersonService,
    private projectService: ProjectService,
  ) {}

  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Scenes.SceneContext) {
    await ctx.reply(`Для создания проекта заполните 3 поля:`);
    await ctx.reply(`1️⃣ Код проекта:`);
    ctx.session['project_form'] = {
      code: '',
      name: '',
      description: '',
    };
  }

  @SceneLeave()
  async onSceneLeave(@Ctx() ctx: Scenes.SceneContext) {
    ctx.session['project_form'] = undefined;
  }

  @WizardStep(0)
  async setCode(
    @Ctx() ctx: Scenes.WizardContext,
    @Message() message: TMessage,
  ) {
    const code = message?.text ?? '';
    const isValid = jiraProjectRegex.test(code);
    const errorMsg = `⚠️ Код проекта должен быть в JIRA формате, от 3 до 6 латинских букв.`;
    if (!code || !isValid) {
      await ctx.reply(errorMsg);
      return;
    }
    ctx.session['project_form'].code = message.text.toUpperCase();
    await ctx.reply(`2️⃣ Имя проекта:`);
    ctx.wizard.next();
  }

  @WizardStep(1)
  async setName(
    @Ctx() ctx: Scenes.WizardContext,
    @Message() message: TMessage,
  ) {
    ctx.session['project_form'].name = message.text;
    await ctx.reply(`3️⃣ Описание проекта:`);
    ctx.wizard.next();
  }

  @WizardStep(2)
  async setDescription(
    @Ctx() ctx: Scenes.WizardContext,
    @Message() message: TMessage,
  ) {
    ctx.session['project_form'].description = message.text;
    const form = ctx.session['project_form'];
    const msg = `Сохранить новый проект ❔
    🔹 <i>Код:</i>  <b>${form.code}</b>
    🔹 <i>Имя:</i>  <b>${form.name}</b>
    🔹 <i>Описание:</i> <b>${form.description}</b>`;
    const keyboard = Markup.inlineKeyboard(
      [
        Markup.button.callback('✔️ Да', SAVE_PROJECT_ACTION),
        Markup.button.callback('✖️ Нет', TO_PROJECTS_ACTION),
      ],
      { columns: 2 },
    );
    await ctx.replyWithHTML(msg, keyboard);
  }

  @Action(SAVE_PROJECT_ACTION)
  async saveNewProject(
    @Ctx() ctx: UpdateContext,
    @Sender('username') username: string,
  ) {
    const form = ctx.session['project_form'];
    const successMsg = (p: Project) => `✅ Проект ${p.code} успешно создан`;
    const errorMsg = (err: Error) => `❌ Проект не создан: ${err.message}`;
    const owner = await this.personService.getByUsername(username);
    if (!owner) return ctx.reply('Ошибка при поиске владельца.');
    const payload = { ...form, owner: owner._id };
    const result = await this.projectService
      .create(payload)
      .catch((err) => err);
    const msg = result?._id ? successMsg(result) : errorMsg(result);
    await ctx.answerCbQuery(msg, { show_alert: true });
    await ctx.scene.enter(PROJECT_LIST_SCENE_ID);
  }

  @Action(TO_PROJECTS_ACTION || 'cancel')
  async cancelNewProject(@Ctx() ctx: UpdateContext) {
    await ctx.answerCbQuery('Возврат к списку проектов');
    await ctx.scene.enter(PROJECT_LIST_SCENE_ID);
  }
}
