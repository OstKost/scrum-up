// Scenes
export const PROJECT_LIST_SCENE_ID = 'SCENE_ID:PROJECT_LIST';
export const PROJECT_CARD_SCENE_ID = 'SCENE_ID:PROJECT_CARD';
export const PROJECT_CREATE_SCENE_ID = 'SCENE_ID:PROJECT_CREATE';
export const PROJECT_UPDATE_SCENE_ID = 'SCENE_ID:PROJECT_UPDATE';
export const PROJECT_USERS_SCENE_ID = 'SCENE_ID:PROJECT_USERS';
export const PROJECT_ADD_USER_SCENE_ID = 'SCENE_ID:PROJECT_ADD_USER';

// Moves
export const TO_PROJECTS_ACTION = 'move:projects_scene';
export const TO_PROJECT_CARD_ACTION = 'move:project_card';
export const TO_CREATE_PROJECT_ACTION = 'move:create_project_scene';
export const TO_UPDATE_PROJECT_ACTION = 'move:update_project_scene';
export const TO_PROJECT_USERS_ACTION = 'move:project_users';

export const LEAVE_SCENE_ACTION = 'move:leave_project_scene';
// Actions
export const SAVE_PROJECT_ACTION = 'action:save_new_project';
export const PROJECT_ADD_USER_ACTION = 'action:add_project_user';
export const PROJECT_REMOVE_USER_ACTION = 'action:remove_project_user';
export const PROJECT_CONFIRM_REMOVE_USER_ACTION =
  'action:confirm_remove_project_user';
export const PROJECT_CANCEL_REMOVE_USER_ACTION =
  'action:cancel_remove_project_user';
