import { Command, Ctx, Hears, Update } from 'nestjs-telegraf';
import { Scenes } from 'telegraf';
import { PROJECT_LIST_SCENE_ID } from './project.constants';
import { UpdateContext } from '../utils/types';

@Update()
export class ProjectUpdate {
  @Command('projects')
  async projects(@Ctx() ctx: Scenes.SceneContext) {
    await ctx.scene.enter(PROJECT_LIST_SCENE_ID);
  }

  @Hears('Мои проекты')
  async toProjects(@Ctx() ctx: UpdateContext) {
    await ctx.scene.enter(PROJECT_LIST_SCENE_ID);
  }
}
