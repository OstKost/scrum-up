import { Module } from '@nestjs/common';
import { CommandsUpdate } from './commands.update';
import { MongooseModule } from '@nestjs/mongoose';
import { Project, ProjectSchema } from '../modules/project/project.model';
import { Person, PersonSchema } from '../modules/person/person.model';
import { PersonModule } from '../modules/person/person.module';
import { ProjectModule } from '../modules/project/project.module';
import { RecordModule } from '../modules/record/record.module';
import { TaskModule } from '../modules/task/task.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Person.name, schema: PersonSchema },
      {
        name: Project.name,
        schema: ProjectSchema,
      },
    ]),
    PersonModule,
    ProjectModule,
    RecordModule,
    TaskModule,
  ],
  providers: [CommandsUpdate],
})
export class CommandsModule {}
