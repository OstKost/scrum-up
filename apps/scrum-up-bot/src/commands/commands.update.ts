import {
  Ctx,
  Start,
  Help,
  On,
  Hears,
  Command,
  Message,
  Action,
  Update,
} from 'nestjs-telegraf';
import { Update as TUpdate } from 'telegraf/typings/core/types/typegram';
import { Context, Markup, Scenes } from 'telegraf';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema as MongooseSchema } from 'mongoose';
import { Person, PersonDocument } from '../modules/person/person.model';
import { Project, ProjectDocument } from '../modules/project/project.model';
import { PersonService } from '../modules/person/person.service';
import { message } from 'telegraf/filters';
import { UpdateContext } from '../utils/types';
import { getUserAnswer } from '../utils/helpers';
import { PROJECT_LIST_SCENE_ID } from '../project/project.constants';
import { TaskService } from '../modules/task/task.service';
import { RecordService } from '../modules/record/record.service';
import dayjs from 'dayjs';

// addUser - Add user to your team
// users - Show all your users
// addProject - Add project
// projects - Show all your projects
// addUserToProject - Add user to project team
// addDailyRecord - Add daily record
// daily - Add daily record
// myDailies - Show all my dailies
// showDailyReport - Show daily records by date
// report - Show daily records by date

@Update()
export class CommandsUpdate {
  constructor(
    // @InjectModel(Project.name) private projectModel: Model<ProjectDocument>,
    private personService: PersonService,
    private taskService: TaskService,
    private recordService: RecordService,
  ) {}

  m = null;

  @Start()
  async start(@Ctx() ctx: Context) {
    const message = ctx.message;
    const payload = {
      username: message.from.username,
      first_name: message.from.first_name,
      last_name: message.from.last_name,
      chatId: message.chat.id,
    };
    let person = await this.personService.getByUsername(payload.username);
    if (!person) {
      person = await this.personService.create(payload);
    }
    const msg = `Добро пожаловать, ${person.first_name} (${person.username})!\nВыберите интересующий пункт:`;
    const keyboard = Markup.keyboard([
      'Daily',
      'Мои записи',
      // 'Отчеты',
      // 'Отгулы и отпуска',
      'Мои задачи',
      'Мои проекты',
    ])
      .oneTime()
      .placeholder('Выберите интересующий пункт:')
      .resize()
      .selective();
    await ctx.reply(msg, keyboard);
  }

  // @Command('projects')
  // async projects(@Ctx() ctx: Context) {
  //   const projects: Project[] = await this.projectModel.find();
  //   let msg = projects.map((p) => `${p.code} - ${p.name}`).join('\n');
  //   if (!msg) msg = 'Проекты не найдены';
  //   await ctx.reply(msg);
  // }

  @Help()
  async help(@Ctx() ctx: Context) {
    await ctx.reply('Send me a sticker');
  }

  // @On('sticker')
  // async on(@Ctx() ctx: Context) {
  //   // await ctx.reply('👍');
  //   await ctx.reply('😘');
  // }
  //
  // @Hears('hi')
  // async hearsHi(@Ctx() ctx: Context) {
  //   const a = await ctx.sendMessage('Hi there');
  //   console.log('DEBUG:', 'commands.update => a', a);
  //   // await ctx.sendDice();
  //   // await ctx.deleteMessage(a.message_id);
  //   // const m = await ctx.;
  //   // console.log('DEBUG:', 'commands.update => m', m);
  //   // await ctx.editMessageText('hilo');
  //   // await ctx.telegram.editMessageText(a.chat.id, undefined, undefined, 'hilo');
  //   // await ctx.answerInlineQuery([
  //   //   { id: '1', title: '123' },
  //   //   { id: '2', title: '234' },
  //   // ]);
  //   const keyboard = Markup.keyboard([
  //     'Дейлик',
  //     'Мои задачи',
  //     'Мои проекты',
  //     'Отгулы и отпуска',
  //   ])
  //     .oneTime()
  //     .placeholder('Выбери свою судьбу:')
  //     .resize()
  //     .selective();
  //   await ctx.reply('Выбери свою судьбу:', keyboard);
  // }

  // @Hears(/.*/)
  // async any(@Ctx() ctx: Context) {
  //   console.log('DEBUG:', 'test.update => ctx.message', ctx.message);
  //   const message_id = ctx.message.message_id;
  //
  //   await setTimeout((r) => r, 2000);
  //
  //   await ctx.reply('👍', {
  //     reply_to_message_id: message_id,
  //     disable_notification: true,
  //   });
  // }

  // @Hears('bye')
  // async hearsBye(@Ctx() ctx: Context) {
  //   this.m = ctx.message;
  //   const keyboard = Markup.inlineKeyboard(
  //     [
  //       Markup.button.callback('Да', 'yes'),
  //       Markup.button.callback('Нет', 'no'),
  //     ],
  //     { columns: 2 },
  //   );
  //   ctx.replyWithHTML(
  //     `Вы действительно хотите добавить задачу:\n\n` + `<i>${'че то там'}</i>`,
  //     keyboard,
  //   );
  // }
  //
  // @Action(['yes', 'no'])
  // async yesNo(
  //   @Ctx()
  //   ctx: Context & {
  //     update: TUpdate.CallbackQueryUpdate;
  //   },
  // ) {
  //   const cbQuery = ctx.update.callback_query;
  //   const userAnswer = 'data' in cbQuery ? cbQuery.data : null;
  //
  //   await ctx.reply(`👌`, {
  //     reply_to_message_id: this.m?.message_id,
  //   });
  //
  //   if (userAnswer === 'yes') {
  //     // addTask('сюда будем передавать текст задачи');
  //     await ctx.editMessageText('Ваша задача успешно добавлена');
  //   } else {
  //     await ctx.deleteMessage();
  //   }
  // }

  //
  // @Hears('scene')
  // async startInitScene(
  //   @Ctx() ctx: Scenes.SceneContext & { update: TUpdate.CallbackQueryUpdate },
  // ) {
  //   await ctx.scene.enter('basicScene');
  // }
  //

  @Hears('Мои задачи')
  async myTasks(@Ctx() ctx: UpdateContext) {
    const user = await this.personService.getByChatId(ctx.chat.id);
    const tasks = await this.taskService.list({ author: user._id });
    if (!tasks || !tasks.length) {
      await ctx.reply('Задачи не найдены');
      return;
    }
    const msg = tasks.map((t) => `${t.code} ${t.title}`).join('\n');
    await ctx.reply('Функционал в разработке');
    await ctx.reply(msg);
  }

  @Hears('Мои записи')
  async myRecords(@Ctx() ctx: UpdateContext) {
    const user = await this.personService.getByChatId(ctx.chat.id);
    const records = await this.recordService.list({
      author: user._id,
      createDateStart: '2000-01-01',
    });
    if (!records || !records.length) {
      await ctx.reply('Записи не найдены');
      return;
    }
    const msg = records
      .map((r) => `${r.task?.code}: ${dayjs(r.updatedAt).format('DD-MM-YYYY')}`)
      .join('\n');
    await ctx.reply('Функционал в разработке');
    await ctx.reply(msg);
  }
}
