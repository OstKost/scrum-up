import { Module } from '@nestjs/common';
import { DailyUpdate } from './daily.update';
import { MongooseModule } from '@nestjs/mongoose';
import { Project, ProjectSchema } from '../modules/project/project.model';
import { Record, RecordSchema } from '../modules/record/record.model';
import { Person, PersonSchema } from '../modules/person/person.model';
import { RecordModule } from '../modules/record/record.module';
import { ProjectModule } from '../modules/project/project.module';
import { DailyScene } from './scenes/daily.scene';
import { TaskModule } from '../modules/task/task.module';
import { PersonModule } from '../modules/person/person.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Person.name,
        schema: PersonSchema,
      },
      {
        name: Project.name,
        schema: ProjectSchema,
      },
      {
        name: Record.name,
        schema: RecordSchema,
      },
    ]),
    TaskModule,
    RecordModule,
    ProjectModule,
    PersonModule,
  ],
  providers: [DailyUpdate, DailyScene],
})
export class DailyModule {}
