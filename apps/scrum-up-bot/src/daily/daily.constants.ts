// Scenes
export const DAILY_SCENE_ID = 'SCENE:DAILY_RECORD';

// Actions
export const CANCEL_ACTION = 'action:cancel';
export const STATUS_TODO_ACTION = 'action:status_todo';
export const STATUS_IN_PROGRESS_ACTION = 'action:status_in_progress';
export const STATUS_DONE_ACTION = 'action:status_done';
export const SEND_ACTION = 'action:send';
