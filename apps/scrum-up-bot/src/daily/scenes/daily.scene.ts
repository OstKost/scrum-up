import {
  Action,
  Ctx,
  Hears,
  Message,
  Scene,
  SceneEnter,
  SceneLeave,
} from 'nestjs-telegraf';
import { Markup, Scenes } from 'telegraf';
import {
  CANCEL_ACTION,
  DAILY_SCENE_ID,
  SEND_ACTION,
  STATUS_DONE_ACTION,
  STATUS_IN_PROGRESS_ACTION,
  STATUS_TODO_ACTION,
} from '../daily.constants';
import { RecordService } from '../../modules/record/record.service';
import { TMessage, UpdateContext } from '../../utils/types';
import { ProjectService } from '../../modules/project/project.service';
import { jiraIssueRegex } from '../../modules/task/task.model';
import { getUserAnswer } from '../../utils/helpers';
import { TaskService } from '../../modules/task/task.service';
import { Project } from '../../modules/project/project.model';
import { PersonService } from '../../modules/person/person.service';
import { InjectConnection } from '@nestjs/mongoose';
import mongoose from 'mongoose';

type RecordData = {
  project: Project;
  taskCode: string;
  status: string;
  text?: string;
};

@Scene(DAILY_SCENE_ID)
export class DailyScene {
  constructor(
    @InjectConnection() private readonly connection: mongoose.Connection,
    private taskService: TaskService,
    private personService: PersonService,
    private recordService: RecordService,
    private projectService: ProjectService,
  ) {}

  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Scenes.SceneContext) {
    const msg = `Введите номер задачи в JIRA формате:`;
    const keyboard = Markup.inlineKeyboard([
      Markup.button.callback('🚫 Отмена', CANCEL_ACTION),
    ]);
    const message = await ctx.reply(msg, keyboard);
    ctx.session['message_id'] = message.message_id;
    ctx.session['record'] = {
      taskCode: null,
      project: null,
      status: null,
      text: null,
    };
  }

  @SceneLeave()
  async onSceneLeave(@Ctx() ctx: Scenes.SceneContext) {
    ctx.session['project'] = undefined;
    ctx.session['message_id'] = undefined;
    ctx.session['record'] = undefined;
  }

  @Hears(jiraIssueRegex)
  async onTaskCode(
    @Ctx() ctx: Scenes.SceneContext,
    @Message() message: TMessage,
  ) {
    const taskCode = message.text;
    const projectCode = taskCode.split('-')[0];
    const project = await this.projectService.getByCode(projectCode);
    if (!project) return ctx.reply(`Проект с кодом ${projectCode} не найден`);

    const taskPayload: RecordData = ctx.session['record'];
    taskPayload.taskCode = taskCode;
    taskPayload.project = project;

    const keyboard = Markup.inlineKeyboard(
      [
        Markup.button.callback('🔵 To Do', STATUS_TODO_ACTION),
        Markup.button.callback('🟡 In Progress', STATUS_IN_PROGRESS_ACTION),
        Markup.button.callback('🟢 Done', STATUS_DONE_ACTION),
        Markup.button.callback('🚫 Отмена', CANCEL_ACTION),
      ],
      { columns: 3 },
    );
    const messageId = ctx.session['message_id'];
    await ctx.telegram.editMessageText(
      message.chat.id,
      messageId,
      undefined,
      `Выберите статус задачи ${message.text} (${project.name})`,
      keyboard,
    );
  }

  @Hears(/.+/)
  async onTaskText(
    @Ctx() ctx: Scenes.SceneContext,
    @Message() message: TMessage,
  ) {
    const text = message.text;
    if (!text) {
      return ctx.reply(
        'Введите какое-нибудь описание или отправьте запись без него.',
      );
    }
    ctx.session['record'].text = text;
    const keyboard = Markup.inlineKeyboard([
      Markup.button.callback('✅ Отправить', SEND_ACTION),
      Markup.button.callback('🚫 Отмена', CANCEL_ACTION),
    ]);
    const messageId = ctx.session['message_id'];
    const recordData: RecordData = ctx.session['record'];
    const { taskCode, status } = recordData;
    const msg = `${taskCode} (${status})\n${text}`;
    await ctx.telegram.editMessageText(
      ctx.chat.id,
      messageId,
      undefined,
      msg,
      keyboard,
    );
  }

  @Action([STATUS_TODO_ACTION, STATUS_IN_PROGRESS_ACTION, STATUS_DONE_ACTION])
  async actionStatus(@Ctx() ctx: UpdateContext) {
    const userAnswer = getUserAnswer(ctx);
    const statuses = {
      [STATUS_TODO_ACTION]: 'TODO',
      [STATUS_IN_PROGRESS_ACTION]: 'IN_PROGRESS',
      [STATUS_DONE_ACTION]: 'DONE',
    };
    ctx.session['record'].status = statuses[userAnswer];

    const keyboard = Markup.inlineKeyboard([
      Markup.button.callback('✅ Отправить', SEND_ACTION),
      Markup.button.callback('🚫 Отмена', CANCEL_ACTION),
    ]);
    const messageId = ctx.session['message_id'];
    const recordData: RecordData = ctx.session['record'];
    const { taskCode, status } = recordData;
    const msg = `${taskCode} (${status})\nДобавьте описание к задаче\nили отправьте в текущем виде`;
    await ctx.telegram.editMessageText(
      ctx.chat.id,
      messageId,
      undefined,
      msg,
      keyboard,
    );
  }

  @Action(CANCEL_ACTION)
  async actionCancel(@Ctx() ctx: Scenes.SceneContext) {
    await ctx.answerCbQuery('Отмена ввода записи о задаче');
    await ctx.scene.leave();
  }

  @Action(SEND_ACTION)
  async actionSend(@Ctx() ctx: Scenes.SceneContext) {
    const recordData: RecordData = ctx.session['record'];
    const chatId = ctx.chat.id;
    const author = await this.personService.getByChatId(chatId);
    if (!author) return ctx.reply(`Пользователь ${chatId} не найден`);

    const transactionSession = await this.connection.startSession();
    transactionSession.startTransaction();
    const taskPayload = {
      code: recordData.taskCode,
      author: author._id,
      project: recordData.project._id,
      status: recordData.status,
    };
    let task = await this.taskService.getByCode(recordData.taskCode);
    if (!task) {
      task = await this.taskService.create(taskPayload);
      await ctx.answerCbQuery(`Задача ${task.code} заведена`);
    }

    const recordPayload = {
      author: author._id,
      project: recordData.project._id,
      task: task._id,
      status: recordData.status,
      text: recordData.text,
    };
    await this.recordService.create(recordPayload);
    await transactionSession.endSession();

    const text = recordData.text || 'Без комментария';
    const msg = `Отчет отправлен\n${recordData.taskCode} (${recordData.status})\n${text}`;
    const messageId = ctx.session['message_id'];
    await ctx.telegram.editMessageText(ctx.chat.id, messageId, undefined, msg);
    await ctx.answerCbQuery(`Статус задачи ${task.code} отправлен`);
    await ctx.scene.leave();
  }
}
