import { Command, Ctx, Update } from 'nestjs-telegraf';
import { DAILY_SCENE_ID } from './daily.constants';
import { UpdateContext } from '../utils/types';

@Update()
export class DailyUpdate {
  @Command(['Daily', 'daily'])
  async daily(@Ctx() ctx: UpdateContext) {
    await ctx.scene.enter(DAILY_SCENE_ID);
  }
}
