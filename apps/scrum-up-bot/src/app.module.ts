import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TelegrafModule } from 'nestjs-telegraf';
import { GreeterBotName } from './app.constants';
import { sessionMiddleware } from './middleware/session.middleware';
import { MongooseModule } from '@nestjs/mongoose';
import { CommandsModule } from './commands/commands.module';
import { DailyModule } from './daily/daily.module';
import { ProjectModule } from './project/project.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env.dev', '.env.prod'],
    }),
    MongooseModule.forRoot(process.env.MONGO_URI),
    TelegrafModule.forRootAsync({
      botName: GreeterBotName,
      useFactory: () => ({
        token: process.env.BOT_TOKEN,
        middlewares: [sessionMiddleware],
        include: [CommandsModule, DailyModule, ProjectModule],
      }),
    }),
    CommandsModule,
    DailyModule,
    ProjectModule,
  ],
  // controllers: [AppController],
  // providers: [PersonModule],
})
export class AppModule {}
