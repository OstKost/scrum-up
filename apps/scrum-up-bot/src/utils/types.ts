import { Context, Scenes } from 'telegraf';
import { Update } from 'telegraf/typings/core/types/typegram';

export type UpdateContext = Scenes.SceneContext & {
  update: Update.CallbackQueryUpdate;
};

export type TMessage = Context['message'] & { text: string };
