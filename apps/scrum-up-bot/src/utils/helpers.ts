import { UpdateContext } from './types';

export const getUserAnswer = (ctx: UpdateContext) => {
  const cbQuery = ctx.update.callback_query;
  return 'data' in cbQuery ? cbQuery.data : null;
};
