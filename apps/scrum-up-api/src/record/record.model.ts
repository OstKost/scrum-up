import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Person } from '../person/person.model';

@ObjectType()
@Schema()
export class Record {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => Person)
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: Person.name })
  author: Person;

  @Field(() => String)
  @Prop()
  project: string;

  @Field(() => String)
  @Prop()
  task: string;

  @Field(() => String)
  @Prop()
  text: string;

  @Field(() => Date)
  @Prop({ default: Date.now() })
  createdAt: Date;

  @Field(() => Date)
  @Prop({ default: Date.now() })
  updatedAt: Date;
}

export type RecordDocument = Record & Document;

export const RecordSchema = SchemaFactory.createForClass(Record);
