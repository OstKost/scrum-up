import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema as MongooseSchema } from 'mongoose';
import dayjs from 'dayjs';
import { Record, RecordDocument } from './record.model';
import {
  CreateRecordInput,
  ListRecordInput,
  UpdateRecordInput,
} from './record.inputs';

@Injectable()
export class RecordService {
  constructor(
    @InjectModel(Record.name) private projectModel: Model<RecordDocument>,
  ) {}

  create(payload: CreateRecordInput) {
    const createdRecord = new this.projectModel(payload);
    return createdRecord.save();
  }

  update(payload: UpdateRecordInput) {
    return this.projectModel
      .findByIdAndUpdate(payload._id, payload, { new: true })
      .exec();
  }

  getById(_id: MongooseSchema.Types.ObjectId) {
    return this.projectModel.findById(_id).exec();
  }

  list(filters: ListRecordInput) {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const { createDateStart, createDateEnd, ...rest } = filters;

    const createdAt = {
      createdAt: {
        $gt: dayjs(filters.createDateStart),
        $lt: dayjs(filters.createDateEnd),
      },
    };

    const payload = { ...rest, ...createdAt };
    return this.projectModel.find(payload).exec();
  }
}
