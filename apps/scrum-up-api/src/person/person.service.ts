import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema as MongooseSchema } from 'mongoose';

import { Person, PersonDocument } from './person.model';
import {
  CreatePersonInput,
  ListPersonInput,
  PersonProjectInput,
  UpdatePersonInput,
} from './person.inputs';
import { ProjectService } from '../project/project.service';

@Injectable()
export class PersonService {
  constructor(
    @InjectModel(Person.name) private personModel: Model<PersonDocument>,
    private readonly projectService: ProjectService,
  ) {}

  create(payload: CreatePersonInput) {
    const createdPerson = new this.personModel(payload);
    return createdPerson.save();
  }

  update(payload: UpdatePersonInput) {
    return this.personModel
      .findByIdAndUpdate(payload._id, payload, { new: true })
      .exec();
  }

  delete(_id: MongooseSchema.Types.ObjectId) {
    return this.personModel.findByIdAndDelete(_id).exec();
  }

  findUser(payload: CreatePersonInput) {
    const { chatId, username } = payload;
    return this.personModel.findOne({ $or: [{ chatId }, { username }] }).exec();
  }

  getById(_id: MongooseSchema.Types.ObjectId) {
    return this.personModel.findById(_id).exec();
  }

  list(filters: ListPersonInput) {
    return this.personModel.find({ ...filters }).exec();
  }

  async addProject({ code, person }: PersonProjectInput) {
    const project = await this.projectService.getByCode(code);
    if (project) {
      return this.personModel.findOneAndUpdate(
        { _id: person },
        { $addToSet: { projects: project._id } },
        { returnDocument: 'after' },
      );
    }
    return new Error('Project not found');
  }

  async deleteProject({ code, person }: PersonProjectInput) {
    const project = await this.projectService.getByCode(code);
    return this.personModel.findOneAndUpdate(
      { _id: person },
      { $pull: { projects: project?._id } },
      { returnDocument: 'after' },
    );
  }
}
