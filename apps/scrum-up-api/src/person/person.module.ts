import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Person, PersonSchema } from './person.model';
import { PersonService } from './person.service';
import { PersonResolver } from './person.resolver';
import { Project, ProjectSchema } from '../project/project.model';
import { ProjectModule } from '../project/project.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Person.name, schema: PersonSchema },
      {
        name: Project.name,
        schema: ProjectSchema,
      },
    ]),
    ProjectModule,
  ],
  providers: [PersonResolver, PersonService],
  exports: [PersonService],
})
export class PersonModule {}
