import { Field, InputType } from '@nestjs/graphql';
import { Schema as MongooseSchema } from 'mongoose';

@InputType()
export class CreatePersonInput {
  @Field(() => String)
  username: string;

  @Field(() => String)
  first_name?: string;

  @Field(() => String)
  last_name?: string;

  @Field(() => Number, { nullable: true })
  chatId?: number;
}

@InputType()
export class ListPersonInput {
  @Field(() => String, { nullable: true })
  _id?: MongooseSchema.Types.ObjectId;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => [String], { nullable: true })
  projects?: MongooseSchema.Types.ObjectId[];
}

@InputType()
export class UpdatePersonInput {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => [String], { nullable: true })
  projects?: MongooseSchema.Types.ObjectId[];
}

@InputType()
export class PersonProjectInput {
  @Field(() => String)
  person: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  code: string;
}
