import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class ListTaskInput {
  @Field(() => String)
  code: string;
}

@InputType()
export class CreateTaskInput {
  @Field(() => String)
  code: string;

  @Field(() => String, { nullable: true })
  title?: string;

  @Field(() => String, { nullable: true })
  description?: string;
}

@InputType()
export class UpdateTaskInput {
  @Field(() => String)
  title: string;

  @Field(() => String)
  description: string;
}
