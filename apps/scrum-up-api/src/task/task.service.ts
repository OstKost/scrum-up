import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Task, TaskDocument } from './task.model';
import { CreateTaskInput, ListTaskInput, UpdateTaskInput } from './task.inputs';

@Injectable()
export class TaskService {
  constructor(@InjectModel(Task.name) private taskModel: Model<TaskDocument>) {}

  create(payload: CreateTaskInput) {
    const createdTask = new this.taskModel(payload);
    return createdTask.save();
  }

  update(code: string, payload: UpdateTaskInput) {
    return this.taskModel.findOneAndUpdate(
      { code },
      { $set: { payload, updatedAt: new Date() } },
    );
  }

  getByCode(code: string) {
    return this.taskModel.findOne({ code }).exec();
  }

  list(filters: ListTaskInput) {
    return this.taskModel.find({ ...filters }).exec();
  }
}
