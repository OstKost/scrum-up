import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import { TaskService } from './task.service';
import { Task } from './task.model';
import { CreateTaskInput, ListTaskInput, UpdateTaskInput } from './task.inputs';

@Resolver(() => Task)
export class TaskResolver {
  constructor(private taskService: TaskService) {}

  @Query(() => [Task])
  async task(@Args('code') code: string) {
    return this.taskService.getByCode(code);
  }

  @Query(() => [Task])
  async tasks(@Args('filters', { nullable: true }) filters?: ListTaskInput) {
    return this.taskService.list(filters);
  }

  @Mutation(() => Task)
  async createTask(@Args('payload') payload: CreateTaskInput) {
    return this.taskService.create(payload);
  }

  @Mutation(() => Task)
  async updateTask(
    @Args('code') code: string,
    @Args('payload') payload: UpdateTaskInput,
  ) {
    return this.taskService.update(code, payload);
  }
}
