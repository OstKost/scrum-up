import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { Task, TaskSchema } from './task.model';
import { TaskService } from './task.service';
import { TaskResolver } from './task.resolver';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Task.name, schema: TaskSchema }]),
  ],
  providers: [TaskResolver, TaskService],
})
export class TaskModule {}
