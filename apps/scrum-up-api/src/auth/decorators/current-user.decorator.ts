import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { Person } from '../../person/person.model';

export const CurrentUser = createParamDecorator(
  (data: unknown, context: ExecutionContext): Person => {
    const ctx = GqlExecutionContext.create(context);
    return ctx.getContext().req.user;
  },
);
