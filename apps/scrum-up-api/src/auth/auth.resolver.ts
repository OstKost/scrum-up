import { Resolver, Args, Mutation, Query } from '@nestjs/graphql';
import { AuthService } from './auth.service';
import { UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { CurrentUser } from './decorators/current-user.decorator';
import { LoginResult } from './dto/login.output';
import { Person } from '../person/person.model';

@Resolver()
export class AuthResolver {
  constructor(private authService: AuthService) {}

  @Mutation(() => LoginResult)
  async login(
    @Args('username') username: string,
    @Args('password') password: string,
  ): Promise<LoginResult> {
    return this.authService.login({ username, password });
  }

  @Query(() => Person)
  @UseGuards(JwtAuthGuard)
  me(@CurrentUser() user: Person): Person {
    return user;
  }
}
