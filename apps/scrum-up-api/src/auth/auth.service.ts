import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { LoginInput } from './dto/login.input';
import { PersonService } from '../person/person.service';
import { Person } from '../person/person.model';

@Injectable()
export class AuthService {
  constructor(
    private personService: PersonService,
    private jwtService: JwtService,
  ) {}

  async validateUser(credentials: {
    username: string;
    password: string;
  }): Promise<Person | null> {
    const user = await this.personService.findUser({
      username: credentials.username,
    });
    const { password } = credentials;
    const hash = user?.password;
    const isMatch = user ? await bcrypt.compare(password, hash) : false;
    if (user && isMatch) {
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...result } = user;
      return result as unknown as Person;
    }
    return null;
  }

  async login(user: LoginInput): Promise<{ access_token: string }> {
    return {
      access_token: this.jwtService.sign(user),
    };
  }

  async validateToken(token: string): Promise<boolean> {
    try {
      this.jwtService.verify(token);
      return true;
    } catch (error) {
      return false;
    }
  }
}
