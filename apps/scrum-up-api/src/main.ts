import { NestFactory } from '@nestjs/core';
import { ScrumUpApiModule } from './scrum-up-api.module';

async function bootstrap() {
  const app = await NestFactory.create(ScrumUpApiModule);
  await app.listen(3000);
}
bootstrap();
