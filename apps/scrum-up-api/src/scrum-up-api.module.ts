import * as path from 'node:path';
import { forwardRef, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { MongooseModule } from '@nestjs/mongoose';
import depthLimit from 'graphql-depth-limit';
import { PersonModule } from './person/person.module';
import { ProjectModule } from './project/project.module';
import { TaskModule } from './task/task.module';
import { RecordModule } from './record/record.module';
import { ReportModule } from './report/report.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env.dev', '.env.prod'],
    }),
    MongooseModule.forRoot(process.env.MONGO_URI),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: path.join(process.cwd(), 'src/schema.gql'),
      sortSchema: true,
      playground: true,
      validationRules: [depthLimit(4)],
    }),
    forwardRef(() => PersonModule),
    forwardRef(() => ProjectModule),
    RecordModule,
    TaskModule,
    ReportModule,
  ],
  controllers: [],
  providers: [],
})
export class ScrumUpApiModule {}
