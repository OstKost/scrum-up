import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema as MongooseSchema } from 'mongoose';

import { Person } from '../person/person.model';

export const jiraProjectRegex = /^[A-Za-z]{3,6}$/;

@ObjectType()
@Schema()
export class Project {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => Person)
  @Prop({ type: MongooseSchema.Types.ObjectId, ref: 'Person' })
  owner: Person;

  @Field(() => String)
  @Prop({
    unique: true,
    validate: jiraProjectRegex,
  })
  code: string;

  @Field(() => String)
  @Prop()
  name: string;

  @Field(() => String)
  @Prop({ default: '' })
  description: string;

  @Field(() => [Person], { nullable: true })
  @Prop({ type: [MongooseSchema.Types.ObjectId], ref: 'Person' })
  users: MongooseSchema.Types.ObjectId[] | Person[];
}

export type ProjectDocument = Project & Document;

export const ProjectSchema = SchemaFactory.createForClass(Project);
