import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { PersonModule } from '@mongo/mongodb/person/person.module';
import { ProjectModule } from '@mongo/mongodb/project/project.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env.local', '.env.dev', '.env.prod'],
    }),
    MongooseModule.forRoot(process.env.MONGO_URI),
    // MongooseModule.forFeature([{ name: Cat.name, schema: CatSchema }]),
    PersonModule,
    ProjectModule,
  ],
  providers: [],
  exports: [],
})
export class MongodbModule {}
