import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Field, ObjectType } from '@nestjs/graphql';
import { Document, Schema as MongooseSchema } from 'mongoose';
import { Project } from '../project/project.model';

@ObjectType()
@Schema()
export class Person {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  @Prop()
  first_name: string;

  @Field(() => String)
  @Prop()
  last_name: string;

  @Field(() => String)
  @Prop()
  username: string;

  @Field(() => [Project])
  @Prop({ type: [MongooseSchema.Types.ObjectId], ref: Project.name })
  projects: MongooseSchema.Types.ObjectId[] | Project[];
}

export type PersonDocument = Person & Document;

export const PersonSchema = SchemaFactory.createForClass(Person);
