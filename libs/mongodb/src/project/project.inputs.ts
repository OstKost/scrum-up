import { Field, InputType } from '@nestjs/graphql';
import { Schema as MongooseSchema } from 'mongoose';

@InputType()
export class CreateProjectInput {
  @Field(() => String)
  ownerID: MongooseSchema.Types.ObjectId;

  @Field(() => String)
  name: string;

  @Field(() => String, { defaultValue: '' })
  description?: string;
}

@InputType()
export class ListProjectInput {
  @Field(() => String)
  ownerID: MongooseSchema.Types.ObjectId;
}

@InputType()
export class UpdateProjectInput {
  @Field(() => String)
  _id: MongooseSchema.Types.ObjectId;

  @Field(() => String, { nullable: true })
  name?: string;

  @Field(() => String, { nullable: true })
  description?: string;
}
